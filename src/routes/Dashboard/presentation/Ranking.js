import React from 'react'

const Ranking = (props) => {
  const { username } = props.userInfo
  return (
    <div className="panel ranking-panel">
      <div className="ranking-header">
        <h2 className="label">Ranking</h2>
        <h2 className="label right">Victorias</h2>
      </div>
      <ul className="ranking-list">
        <li>
          <span className="number">01</span>
          <span className="name">Terminator 4000</span>
          <span className="victories xl">324</span>
        </li>
        <li>
          <span className="number">02</span>
          <span className="name">Robocop</span>
          <span className="victories">320</span>
        </li>
        <li>
          <span className="number">03</span>
          <span className="name">Darth Vader</span>
          <span className="victories">310</span>
        </li>
        <li className="current">
          <span className="number">05</span>
          <span className="name">{username}</span>
          <span className="victories">300</span>
        </li>
        <li>
          <span className="number">06</span>
          <span className="name">GrumpyLion</span>
          <span className="victories">299</span>
        </li>
        <li>
          <span className="number">07</span>
          <span className="name">Werelax</span>
          <span className="victories">298</span>
        </li>
      </ul>
    </div>
  )
}

export default Ranking
