import React from 'react'

const Header = () => {
  return (
    <header className="main-header">
      <a href="#" className="logo"><img alt="Bettr" src="../assets/images/logo.png" /></a>
      <div className="logout">
        <a href="/logout" className="logout-button">
          <span className="icon icon-logout"></span>
        </a>
      </div>
    </header>
  )
}

export default Header
