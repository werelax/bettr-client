import React from 'react'


export const GeneralLayout = ({ children }) => (
  <div classNameName='container text-center'>
    {children}
  </div>
)

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default CoreLayout
