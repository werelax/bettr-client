import Dashboard from './Dashboard'

export const createRoutes = (store) => ({
  path: '/',
  indexRoute: Dashboard,
  childRoutes: [
    Dashboard
  ]
})

export default createRoutes
