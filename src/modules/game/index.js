import io from 'socket.io-client'
import { apiHost } from 'config'

export let socket = undefined
export let store = undefined
let socketConnected = false

export let gameWorld = {
  blocks: [],
  player: {
    position: {x: 0, y: 0}
  }
}

export const initSocket = (storeInstance) => {
  socket = io(apiHost)
  store = storeInstance;

  socket.on('connect', () => {
    socketConnected = true
    console.log('* [socket] connected!!')
  })

  socket.on('START_MATCH', () => {
    console.log('* [socket] start match');
    gameWorld = undefined;
    store.dispatch(actions.startMatch())
  })

  socket.on('END_MATCH', () => {
    console.log('* [socket] end match');
    store.dispatch(actions.endMatch())
  })

  socket.on('GAME_EVENT', (payload) => {
    console.log('* [socket] game event');
    gameWorld = payload;
    // store.dispatch(actions.gameEvent(payload))
  })

  socket.on('RESET_PLAYER', () => {
    console.log('* [socket] reset player');
    store.dispatch(actions.resetPlayer())
  })

}

export const emit = (event, ...args) => {
  if (socketConnected)
    socket.emit(event, ...args)
  else
    setTimeout(() => emit(event, ...args), 300)
}

//-------------------------
// actions

const actionTypes = {
  GAME_FIND_MATCH: 'GAME_FIND_MATCH',
  GAME_START_MATCH: 'GAME_START_MATCH',
  GAME_END_MATCH: 'GAME_END_MATCH',
  GAME_EVENT: 'GAME_EVENT',
  GAME_RESET_PLAYER: 'GAME_RESET_PLAYER'
}

export const gameStatuses = {
  STOPED: 'STOPED',
  SEARCHING: 'SEARCHING',
  PLAYING: 'PLAYING'
}

export const actions = {
  registerPlayer: (userInfo) => () => {
    console.log('register player', userInfo)
    emit('REGISTER_PLAYER', userInfo)
  },
  findMatch: () => (dispatch) => {
    console.log('findMatch!')
    emit('FIND_MATCH')
    dispatch(actions.doFindMatch())
  },
  defeat: () => () => {
    emit('DEFEAT')
  },
  doFindMatch: () => ({
    type: actionTypes.GAME_FIND_MATCH
  }),
  startMatch: () => ({
    type: actionTypes.GAME_START_MATCH
  }),
  endMatch: () => ({
    type: actionTypes.GAME_END_MATCH
  }),
  gameEvent: (payload) => ({
    type: actionTypes.GAME_EVENT,
    payload: payload
  }),
  resetPlayer: () => ({
    type: actionTypes.GAME_RESET_PLAYER
  })
}

const ACTION_HANDLERS = {
  [actionTypes.GAME_FIND_MATCH]: (state, action) => {
    console.log('GAME_FIND_MATCH')
    return Object.assign({}, state, {status: gameStatuses.SEARCHING})
  },
  [actionTypes.GAME_START_MATCH]: (state, action) => {
    console.log('GAME_START_MATCH')
    return Object.assign({}, state, {status: gameStatuses.PLAYING})
  },
  [actionTypes.GAME_END_MATCH]: (state, action) => {
    console.log('GAME_END_MATCH')
    return Object.assign({}, state, {status: gameStatuses.STOPED})
  },
  [actionTypes.GAME_EVENT]: (state, action) => {
    console.log('GAME_EVENT')
    return state
  },
  [actionTypes.GAME_RESET_PLAYER]: (state, action) => {
    console.log('GAME_RESET_PLAYER')
    return Object.assign({}, state, {status: gameStatuses.STOPED})
  }
}

//-------------------------
// reducer

//-------------------------
// reducer
const initialState = {
  world: {},
  status: gameStatuses.STOPED
}
export const reducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

