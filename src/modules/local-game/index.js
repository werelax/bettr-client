import { gameWorld, socket, store, actions } from 'modules/game'

export function createGame (id) {

  var TOTAL_BGS = 22;

  var game = new Phaser.Game(
    800, 600,
    Phaser.AUTO,
    id,
    {
      preload: preload,
      create: create,
      update: update,
      render: render
    });

  var bgNumber = game.rnd.integerInRange(1, 21);

  function preload() {
    game.load.image('bg', 'bgs/bg' + bgNumber + '.png');
    game.load.image('block', 'block.png');
    game.load.image('mushroom', 'mushroom2.png');
  }

  var player;
  var blocks;
  var cursors;

  var PLAYER_VELOCITY = 650;
  var PLAYER_WIDTH = 64;
  var MAX_BLOCKS = 5;
  var obstacleSpeed = 500;
  var playerSpeed = PLAYER_VELOCITY;
  var all = 0;
  var state = {};

  function create() {

    // BG
    game.add.image(0, 0, 'bg');


    // BLOCKS
    createBlocks();

    // PLAYER
    player = game.add.sprite((game.width / 2) - (PLAYER_WIDTH / 2), game.height - 100, 'mushroom');
    player.name = 'mushroom';
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.immovable = true;

    // KEYS
    cursors = game.input.keyboard.createCursorKeys();


    // GAME STATE
    state = {
      blocks: blocks.children.map(function (block) {
        return {
          position: {
            x: block.position.x,
            y: block.position.y
          }
        };
      }),
      player: {
        position: {
          x: player.position.x,
          y: player.position.y
        }
      }
    };

    console.log('EMIT');
    socket.emit('GAME_EVENT', state);
  }

  function update() {
    player.body.velocity.x = 0;

    blocks.forEach(function (block) {
      game.physics.arcade.collide(block, player, collisionHandler, null, this);
    });

    if (cursors.left.isDown) {
      player.body.velocity.x = -playerSpeed;
    } else if (cursors.right.isDown) {
      player.body.velocity.x = playerSpeed;
    }

    blocks.forEach(checkOffScreen, false);

    if (all === MAX_BLOCKS) {
      all = 0;
      obstacleSpeed = obstacleSpeed * 1.10;
      playerSpeed = playerSpeed * 1.10;
    }

    state = {
      blocks: blocks.children.map(function (block) {
        return {
          position: {
            x: block.position.x,
            y: block.position.y
          }
        };
      }),
      player: {
        position: {
          x: player.position.x,
          y: player.position.y
        }
      }
    };

    socket.emit('GAME_EVENT', state);
  }

  function checkOffScreen(block) {
    if (block.body.y > game.height) {
      block.body.x = game.rnd.integerInRange(0, 736);
      block.body.y = -400;
      // block.body.velocity.x = 0;
      block.body.velocity.y = obstacleSpeed;
      all++;
    }
  }

  function collisionHandler (obj1, obj2) {
    // game.stage.backgroundColor = '#992d2d';
    // player.body.velocity.y = 0;
    game.destroy();
    store.dispatch(actions.defeat());
  }

  function createBlocks() {
    blocks = game.add.group();
    blocks.y = -400;

    for (var i = 0; i < MAX_BLOCKS; i++) {
      blocks.create(game.rnd.integerInRange(0, 736), game.rnd.integerInRange(0, 100), 'block');
    }

    game.physics.enable(blocks, Phaser.Physics.ARCADE);

    blocks.forEach(function (block) {
      block.body.velocity.y = obstacleSpeed;
    });

  }

  function render(){}

  return game;
}
