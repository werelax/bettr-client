import React from 'react'
import Header from '../presentation/Header'
import { createGame as createLocalGame } from 'modules/local-game'
import { createGame as createRemoteGame } from 'modules/remote-game'

class GameScreen extends React.Component {

  componentDidMount () {
    this.localGame = createLocalGame('panel1')
    this.remoteGame = createRemoteGame('panel2')
  }

  render () {
    const {defeat} = this.props
    return (
      <div>
        <div className="main-content">
          <Header />

          <div className="game-content full-content">
            <div className="user-panel" id="panel1"></div>
            <div className="user-panel" id="panel2"></div>
          </div>

        </div>
      </div>
    )
  }
} 

export default GameScreen
