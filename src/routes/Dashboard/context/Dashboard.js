import React from 'react'
import { connect } from 'react-redux'
import { actions as userInfoActions } from 'modules/user-info'
import { actions as gameActions } from 'modules/game'

import DashboardLayout from '../composition/Dashboard'

const mapActionCreators = {
  getUserInfo: userInfoActions.getUserInfo,
  registerPlayer: gameActions.registerPlayer,
  findMatch: gameActions.findMatch,
  endMatch: gameActions.endMatch,
  defeat: gameActions.defeat,
  addCash: userInfoActions.addCash
}

const mapStateToProps = (state) => ({
  userInfo: state.userInfo,
  gameStatus: state.game.status
})

class DashboardContext extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    const { getUserInfo, registerPlayer } = this.props
    getUserInfo()
      .then((userInfo) => {
        console.log('then', userInfo)
        registerPlayer(userInfo)
      })
  }

  render () {
    return (
      <DashboardLayout
         {...this.props}
         addCash={this.props.addCash}
         />
    )
  }

}

export default connect(mapStateToProps, mapActionCreators)(DashboardContext)
