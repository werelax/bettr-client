import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import { reducer as userInfo } from 'modules/user-info'
import { reducer as game } from 'modules/game'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    router,
    userInfo,
    game,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
