import React from 'react'

class ReplaceableTabs extends React.Component {
  constructor (props) {
    super(props)
    this.state = {selected: props.root};
    this.switchTab = this.switchTab.bind(this)
    this.showRoot = this.showRoot.bind(this)
  }

  switchTab (childIndex) {
    const selected = this.props.children[childIndex]
    return () => this.setState({selected})
  }

  showRoot () {
    const selected = this.props.root
    return () => this.setState({selected})
  }

  render () {
    const SelectedComponent = this.state.selected;
    return (
      <SelectedComponent
         switchTab={this.switchTab}
         showRoot={this.showRoot}
         {...this.props}
         />
    )
  }
}

export default ReplaceableTabs;
