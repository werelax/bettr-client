import React from 'react'

const Suministros = () => {
  return (
    <div className="panel buy-panel">
      <h2 className="label">Comprar suministros</h2>
      <ul className="buy-list">
        <li>
          <img className="badge-img" alt="badge" src="../assets/images/badge-worm.svg" />
          <div className="buy-info">
            <h3 className="buy-title">Worm</h3>
            <p>Reduce la velocidad de tu contrincante</p>
            <a href="#" className="rounded-button">0,10€</a>
          </div>
        </li>
        <li>
          <img className="badge-img" alt="badge" src="../assets/images/badge-storm.svg" />
          <div className="buy-info">
            <h3 className="buy-title">Storm</h3>
            <p>Haz que tu contrincante vibre durante unos segundos</p>
            <a href="#" className="rounded-button">0,05€</a>
          </div>
        </li>
        <li>
          <img className="badge-img" alt="badge" src="../assets/images/badge-freezing.svg" />
          <div className="buy-info">
            <h3 className="buy-title">Freezing</h3>
            <p>Congela a tu contrincante durante unos segundos</p>
            <a href="#" className="rounded-button">0,20€</a>
          </div>
        </li>
        <li>
          <img className="badge-img" alt="badge" src="../assets/images/badge-x2.svg" />
          <div className="buy-info">
            <h3 className="buy-title">x2</h3>
            <p>Tus victorias sumarán 2 puntos</p>
            <a href="#" className="rounded-button">0,05€</a>
          </div>
        </li>
      </ul>
    </div>
  )
}

export default Suministros
