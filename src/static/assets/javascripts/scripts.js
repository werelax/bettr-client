$(function(){

	///////////// CUSTOM FORMS

	$('input[type=checkbox], input[type=radio], select').custom_form({
		responsive_select:true
	});

	///////////// SUBMENU OPEN

	$('.has-submenu').on('click', function() {
		var $submenuTitle = $(this).find('.submenu-title');
	  $('.has-submenu').not(this).removeClass('open-menu');
		$('.submenu-title').not($submenuTitle).removeClass('is-active');
		$(this).toggleClass('open-menu');
		$submenuTitle.toggleClass('is-active');
	});

	///////////// MOBILE MENU OPEN
	$('.mobile-icon').on('click', function() {
		$(this).closest('.outer-wrapper').toggleClass('mobile-reveal-menu');
	});

	///////////// MOBILE MENU OPEN
	$('.filters-actions .button-secondary').on('click', function(e) {
		e.preventDefault();
		$(this).closest('.filters').toggleClass('collapsed');
		$('.filters-actions').toggleClass('toggle');
	});


  /////////// FIXABLE HEADER SCRIPT
	$(window).on('scroll', (function() {
		var topThreshold = undefined;
		return function () {
			var $fixableElement = $('.fixable');
			if ($fixableElement.length === 0) return;
			var $container = $fixableElement.parent();
			var scrollTop = $(window).scrollTop();
			topThreshold = topThreshold || $fixableElement.offset().top;
			var isAboveThreshold = scrollTop > topThreshold;
			var fixableElementHeight = $fixableElement.height();
			$fixableElement.toggleClass('is-fixed', isAboveThreshold);
			$container.css('margin-top', isAboveThreshold ? fixableElementHeight : '');
		}
	}()));

});
