export const formatMoney = (money) => {
  const eur = Math.floor(money / 100)
  const cents = ((money % 100).toString() + '00').slice(0, 2)
  return `${eur}.${cents}`
}
