import React from 'react'

const UserBox = (props) => {
  const { username, credit } = props.userInfo
  return (
    <div className="user-box">
      <div className="user-profile">
        <img alt="user" src="../assets/images/avatar.png" />
        <div className="user-info">
          <p className="user-name">{ username }</p>
          <p className="user-leage">Liga de plata</p>
        </div>
      </div>
      <div className="user-badges">
        <ul className="badges">
          <li className="inactive">
            <img alt="badge" src="../assets/images/badge-freezing.svg" />
            <span className="badge-number">0</span>
          </li>
          <li>
            <img alt="badge" src="../assets/images/badge-storm.svg" />
            <span className="badge-number">2</span>
          </li>
          <li>
            <img alt="badge" src="../assets/images/badge-worm.svg" />
            <span className="badge-number">1</span>
          </li>
          <li className="inactive">
            <img alt="badge" src="../assets/images/badge-x2.svg" />
            <span className="badge-number">0</span>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default UserBox
