import request from 'superagent'

const reqHandler = (resolve, reject) =>
        (err, res) => (err === null) ? resolve(res.body) : reject(err)

const storage = window.localStorage

// Elias, are you OK with us doing it this way?
const getApiToken = () =>
  (storage.getItem('currentUser') || {}).api_key

export const get = (url, params) => {
  return new Promise((resolve, reject) => {
    request
      .get(url)
      .set('Authorization', getApiToken())
      .set('Accept', 'application/json')
      .query(params)
      .end(reqHandler(resolve, reject))
  })
}

export const post = (url, params) => {
  return new Promise((resolve, reject) => {
    request
      .post(url)
      .set('Authorization', getApiToken())
      .set('Accept', 'application/json')
      .send(params)
      .end(reqHandler(resolve, reject))
  })
}

export const put = (url, params) => {
  return new Promise((resolve, reject) => {
    request
      .put(url)
      .set('Authorization', getApiToken())
      .set('Accept', 'application/json')
      .send(params)
      .end(reqHandler(resolve, reject))
  })
}

export const del = (url) => {
  return new Promise((resolve, reject) => {
    request
      .del(url)
      .set('Authorization', getApiToken())
      .set('Accept', 'application/json')
      .end(reqHandler(resolve, reject))
  })
}
