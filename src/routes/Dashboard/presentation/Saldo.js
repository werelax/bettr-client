import React from 'react'

import { formatMoney } from 'utils.js'

const Saldo = (props) => {
  const {credit} = props.userInfo
  return (
    <div className="panel">
      <div className="info">
        <div className="label">Saldo</div>
        <strong className="number">{formatMoney(credit)}€</strong>
      </div>
      <div className="balance-buttons">
        <a href="#" className="button button-secondary">Añadir saldo</a>
        <a href="#" className="button button-link">Retirar saldo</a>
      </div>
    </div>
  )
}

export default Saldo
