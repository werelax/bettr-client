import React from 'react'
import ReplaceableTabs from 'components/interaction/ReplaceableTabs'

import Header from '../presentation/Header'
import Saldo from '../presentation/Saldo'
import Suministros from '../presentation/Suministros'
import Ranking from '../presentation/Ranking'
import UserBox from '../presentation/UserBox'
import PlayerActions from '../presentation/PlayerActions'
import GameScreen from '../context/GameScreen'

import { gameStatuses } from 'modules/game'
import { formatMoney } from 'utils.js'

export const Dashboard = (props) => {
  const { userInfo, findMatch, gameStatus } = props
  if (gameStatus === gameStatuses.PLAYING) {
    return (
      <GameScreen {...props} />
    )
  } else {
    return (
      <div className="main-content">
        <Header />

        <div className="dashboard-content">
          <div className="wrapper">
            <UserBox userInfo={ userInfo } />
            <div className="g row">
              <div className="gi one-third">
                <Saldo userInfo={userInfo}/>
                <Suministros />
              </div>
              <div className="gi two-thirds">
                <PlayerActions findMatch={findMatch}
                               gameStatus={gameStatus}/>
                <Ranking userInfo={userInfo} />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Dashboard
