/* global process */

const base = {
  apiPrefix: '/api/v1'
}

const config = {
  development: Object.assign({
    serverHost: 'http://10.102.83.41:3000',
    apiHost: 'http://10.102.83.41:3001'
  }, base),

  test: Object.assign({
    apiHost: 'http://localhost:3001'
  }, base),

  production: Object.assign({
    serverHost: 'http://45.79.154.72',
    apiHost: 'http://45.79.154.72'
  }, base)
}

const env = process.env.NODE_ENV

export const apiHost = config[env].apiHost
export const apiPrefix = config[env].apiPrefix
export const serverHost = config[env].serverHost
