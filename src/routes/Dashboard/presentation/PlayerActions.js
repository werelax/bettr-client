import React from 'react'
import { gameStatuses } from 'modules/game'

const PlayerActions = (props) => {
  const { findMatch, gameStatus } = props
  const loading = gameStatus === gameStatuses.SEARCHING
  return (
    <div className="panel">
      <div className="banner">
        <span className="icon icon-trophy"></span>
        <div className="banner-info">
          <h2 className="beta">¡Ya casi lo tenemos!</h2>
          <p className="lead-text">Sólo te faltan 2 partidas para entrar en la liga de oro</p>
        </div>
      </div>

      <a className={'button button-wide ' + (loading? '' : 'button-primary')}
         style={{cursor: 'pointer'}}
         onClick={findMatch}
         >
        { loading ? 'Buscando Jugadores...' : 'Jugar ahora!'}
      </a>
      <div className="buttons-group">
        <a href="#" className={'button button-flat active ' + (loading? 'loading' : '')}>
          <span className="icon icon-balance"></span>
          <span className="text">Same level</span>
          {loading && <img className="loader" alt="loader" src="../assets/images/loader.gif" />}
        </a>
        <a href="#" className="button button-flat">
          <span className="icon icon-mix"></span>
          <span className="text">Aleatorio</span>
          
        </a>
        <a href="#" className="button button-flat">
          <span className="icon icon-user"></span>
          <span className="text">Retar a alguien</span>
        </a>
      </div>
    </div>
  )
}

export default PlayerActions
