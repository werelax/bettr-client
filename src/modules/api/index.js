import * as config from 'config'
import { get, post, upload, put, del } from './ajax'

// -------------------------
// config

export const API_PREFIX = config.apiHost + config.apiPrefix

export const genericPost = (url, data) => {
  const serverUrl = API_PREFIX + url
  return post(serverUrl, data)
}

export const genericUpload = (url, data) => {
  const serverUrl = API_PREFIX + url
  return upload(serverUrl, data)
}

export const fetchOne = (resource) => {
  return (resourceParams) => {
    const url = API_PREFIX + resource(resourceParams)
    return get(url)
  }
}

export const fetchMany = (resource) => {
  return (resourceParams = {}) => {
    const url = API_PREFIX + resource({...resourceParams})
    return get(url, {})
  }
}

export const postOne = (resource) => {
  return (resourceParams, data) => {
    const url = API_PREFIX + resource(resourceParams)
    return post(url, data)
  }
}

export const putOne = (resource) => {
  return (resourceParams, data) => {
    const url = API_PREFIX + resource(resourceParams)
    return put(url, data)
  }
}

export const deleteOne = (resource) => {
  return (resourceParams) => {
    const url = API_PREFIX + resource(resourceParams)
    return del(url)
  }
}
