import { get } from 'modules/api/ajax'
import { apiHost } from 'config'

// actions
const actionTypes = {
  USER_INFO_UPDATE: 'USER_INFO_UPDATE',
  USER_ADD_CASH: 'USER_ADD_CASH',
  USER_SET_CASH: 'USER_SET_CASH'
}

export const actions = {
  getUserInfo: () => (dispatch, getState) => {
    console.log('getting user info...')
    // get(`${apiHost}/me`)
    return get(`${apiHost}/debug-sign-me-up`)
      .then((userInfo) => {
        dispatch(actions.userInfoUpdate(userInfo))
        return userInfo
      })
  },
  userInfoUpdate: (userInfo) => ({
    type: actionTypes.USER_INFO_UPDATE,
    payload: userInfo
  }),
  addCash: (amount) => ({
    type: actionTypes.USER_ADD_CASH,
    payload: amount
  }),
  setCash: (amount) => ({
    type: actionTypes.USER_SET_CASH,
    payload: amount
  })
}

const ACTION_HANDLERS = {
  [actionTypes.USER_INFO_UPDATE]: (state, action) => {
    console.log('user info updated', action.payload)
    return Object.assign({}, state, action.payload)
  },
  [actionTypes.USER_ADD_CASH]: (state, action) => {
    console.log('addCash', action.payload)
    return Object.assign({}, state, {cash: state.cash + action.payload})
  },
  [actionTypes.USER_SET_CASH]: (state, action) => {
    return Object.assign({}, state, {cash: action.payload})
  }
}

//-------------------------
// reducer
const initialState = {
  username: '',
  token: '',
  cash: 0
}
export const reducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
